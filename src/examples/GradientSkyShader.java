/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class GradientSkyShader extends SimpleApplication {


    private Node sky;
    private Geometry background;
    
    private float skySphereRadius = DEFAULT_SKYRADIUS;                          // sky sphere radius
    
    
    private static final Vector3f TOP_SKY_COLOR = new Vector3f(.004f, .016f, .046f);
    private static final Vector3f BOTTOM_SKY_COLOR = new Vector3f(.005f, .102f, .334f);
    
    private static final float DEFAULT_SKYRADIUS = 50f;

    
    
    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        cam.setLocation(new Vector3f(0f, 0f, 5f));
        
        viewPort.setBackgroundColor(ColorRGBA.LightGray);

        
        loadModels();
        
    }
    
    
    // http://hub.jmonkeyengine.org/t/background-gradient/14605/29
    // http://stackoverflow.com/questions/30085923/how-can-i-make-this-spherical-gradient-in-an-opengl-fragment-shader
    
    // todo:
    private void setupSkySphere(Node pivot) {
        
        background = new Geometry("Background", new Sphere(128, 128, skySphereRadius));
        
        Material material = new Material(assetManager, "MatDefs/GradientSky.j3md");
        material.setVector3("TopColor", TOP_SKY_COLOR);        
        material.setVector3("BottomColor", BOTTOM_SKY_COLOR);
//        material.setTexture("Texture", assetManager.loadTexture("Blender/starfield.dds"));
//        material.setFloat("Alpha", 1f);
        material.setBoolean("Stars", true);
        material.setVector2("Resolution", new Vector2f(this.settings.getWidth(), this.settings.getHeight()));
        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Front);
        background.setMaterial(material);     
        
 
        pivot.attachChild(background);
        
    }
    
    // Load scene
    private void setupScene() {
        
        Spatial earth = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        rootNode.attachChild(earth);  
        
    }
    
    // todo:
    private void loadModels() {
        
        sky = new Node("Sky");
        sky.setLocalTranslation(Vector3f.ZERO);
        rootNode.attachChild(sky);
        
        setupSkySphere(sky);
//    setupScene();
        // todo: add sky control
        
    }
    
    int i = 0;
    @Override
    public void simpleUpdate(float tpf) {
         
        sky.rotate(FastMath.DEG_TO_RAD * tpf, 0f, 0f);
        
        i++;
        if(i == 50) background.getMaterial().setTexture("Texture", null);       // remove texture
        
    }

    public static void main(String[] args) {
        
        GradientSkyShader app = new GradientSkyShader();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(960, 540);                   // original 540, 960
        settings.setFrameRate(20);
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();
        
    }

}