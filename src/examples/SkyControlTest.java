package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;
import jme3utilities.sky.SkyControl;

/**
 * test
 * @author alex
 */



public class SkyControlTest extends SimpleApplication {

    private Geometry sun;
    private Geometry moon;
    private Node pivot;
    
    private DirectionalLight light;
    
    private SkyControl sky;
    
    private static final int SHADOW_MAP_SIZE = 1024;

    
    // Planet rotation control
    public class PlanetControl extends AbstractControl {
        
        private static final float PLANET_SPEEDFACTOR = 0.1f;


        @Override
        protected void controlUpdate(float tpf) {

            spatial.rotate(new Quaternion().fromAngleAxis(PLANET_SPEEDFACTOR * tpf, Vector3f.UNIT_X));
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) { }
        
    }
    
    
    @Override
    public void simpleInitApp() {
        
        setupCam();        
        setupLight();
        
        setupPlanetSystem();
        
        // Load scene
        Spatial earth = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        earth.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        rootNode.attachChild(earth);  
       
        
        // Control
        pivot.addControl(new PlanetControl());

        // Sky
        sky = new SkyControl(assetManager, cam, 0f, true, false);
        rootNode.addControl(sky);
        sky.setEnabled(true);        
        sky.getUpdater().setMainLight(light);
        
        
    }

    
    private void setupPlanetSystem() {
        
        // Pivot
        pivot = new Node("Pivot");
        
        // Sun 
        sun = new Geometry("Sun", new Sphere(8, 8, 1f));
        
        sun.setQueueBucket(RenderQueue.Bucket.Sky);
        sun.setCullHint(Spatial.CullHint.Never);
        sun.setShadowMode(RenderQueue.ShadowMode.Off);
        
        Material sunMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        sunMat.setBoolean("UseMaterialColors", true);
        sunMat.setColor("Ambient", ColorRGBA.Yellow);
        sunMat.setColor("Diffuse", ColorRGBA.Yellow);
        sun.setMaterial(sunMat);
        
        // Moon
        moon = new Geometry("Moon", new Sphere(8, 8, 1f));
        
        moon.setQueueBucket(RenderQueue.Bucket.Sky);
        moon.setCullHint(Spatial.CullHint.Never);
        moon.setShadowMode(RenderQueue.ShadowMode.Off);
        
        Material moonMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        moonMat.setBoolean("UseMaterialColors", true);
        moonMat.setColor("Ambient", ColorRGBA.LightGray);
        moonMat.setColor("Diffuse", ColorRGBA.LightGray);
        moon.setMaterial(moonMat);
        
        // Positioning
        sun.setLocalTranslation(0f, 15f, 0f);
        moon.setLocalTranslation(0f, -15f, 0f);
        pivot.attachChild(sun);
        pivot.attachChild(moon);
        rootNode.attachChild(pivot);
        
    }
       
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        
        cam.setLocation(new Vector3f(6.2f, 7f, 4.8f));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
        
    }
    
    private void setupLight() {    
         
        light = new DirectionalLight();     
        rootNode.addLight(light);
        
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 2);
        dlsr.setLight(light);        
        viewPort.addProcessor(dlsr);
        
        
    }

    float hour = 0f;
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
        if(hour == 24f) hour = 0f;
        sky.getSunAndStars().setHour(hour);
        hour += 0.1f * tpf;
        
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        SkyControlTest app = new SkyControlTest();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
