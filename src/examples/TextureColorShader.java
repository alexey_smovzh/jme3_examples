/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class TextureColorShader extends SimpleApplication {
    
    private Material material;    
    private static final int STEPS = 20;
    
    private Geometry geometry;
    

    @Override
    public void simpleInitApp() {
        
        cam.setLocation(new Vector3f(0f, 0f, 20f));
        flyCam.setMoveSpeed(40f);
        viewPort.setBackgroundColor(ColorRGBA.LightGray);
        

        Box mesh = new Box(12f, 3.36f, 0.1f);
        geometry = new Geometry("Plane", mesh);
        
        material = new Material(assetManager, "MatDefs/TextureColor.j3md");
        material.setTexture("Texture", assetManager.loadTexture("Blender/TextureColorShader/Plane2.png"));
        geometry.setMaterial(material);
        
        rootNode.attachChild(geometry);
               
    }
    
    private static final Vector3f min = new Vector3f(.92f, .93f, .97f);                        
    private static final Vector3f mask = new Vector3f(.84f, .77f, .67f);
    // Get color for texture multiplying based on currentStep
    private ColorRGBA getColor(int step) {
        
        Vector3f result = min.subtract(new Vector3f((mask.x / STEPS) * step,
                                                    (mask.y / STEPS) * step,
                                                    (mask.z / STEPS) * step));
        
        return new ColorRGBA(result.x, result.y, result.z, 0f);
        
    }
            
    int i = 0;
    private int step = 0;
    
    @Override
    public void simpleUpdate(float tpf) {
    
        i++;
        
        if(i > 100 && i < 120) {        // make night
            step++;
            
            material.setColor("Color", getColor(step));
        } 
        
        if(i == 140) {                  // light bonfire
            
            Texture bonfire = assetManager.loadTexture("Blender/TextureColorShader/Plane2_nf.png");
            geometry.getMaterial().setTexture("BonfireLight", bonfire);
            
            step = STEPS;
            
        }
        
        if(i == 280) {                  // off bonfire light
            
            geometry.getMaterial().clearParam("BonfireLight");
            
        }
        
        if(i > 300 && i < 320) {        // make day

            step--;
            
            if(step == 0) { 
                material.clearParam("Color");
            } else {
                material.setColor("Color", getColor(step));                
            }
        } 
        
    }

    public static void main(String[] args) {
        
        TextureColorShader app = new TextureColorShader();

        AppSettings settings = new AppSettings(true);
        settings.setResolution(960, 540);                   // original 540, 960
        settings.setFrameRate(20);
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();
        
    }
    
}
