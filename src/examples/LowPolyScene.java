package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowFilter;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.system.AppSettings;



public class LowPolyScene extends SimpleApplication {

    private PointLightShadowRenderer renderer;
    private PointLightShadowFilter filter;
    
    private int SHADOW_MAP_SIZE = 1024;
   

    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.DarkGray);

        

        
        Node scene = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");        
        rootNode.attachChild(scene);

        // Point Light
        PointLight lamp = new PointLight();
        lamp.setColor(ColorRGBA.White.mult(5f));
        lamp.setRadius(20f);
        lamp.setPosition(new Vector3f(4f, 4f, -3f));
        rootNode.addLight(lamp);
        
        
        renderer = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        renderer.setLight(lamp);
        renderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        viewPort.addProcessor(renderer);
        
        filter = new PointLightShadowFilter(assetManager, SHADOW_MAP_SIZE);
        filter.setLight(lamp);
        filter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        filter.setEnabled(false);
        
        // Fpp
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);


      
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        LowPolyScene app = new LowPolyScene();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(800, 600);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
