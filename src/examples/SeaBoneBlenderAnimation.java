package examples;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.SimpleApplication;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowFilter;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.system.AppSettings;

/*
 * 1. в блендере создаем поверхность, разбиваем на сегменты
 * 2. в режиме редактирования выделяем вершину, создаем вертекс группу и даем ей имя wave1. 
 *    повторяем для всех нужных вершин.
 * 3. в объектном режиме создаем кость. выделяем ее, переходим режим редактирования. 
 *    по shift + A создаем новые кости по количеству созданных ранее вертекс групп
 * 4. даем имена костям такие же как и вертекс групп wave1, wave2...
 * 5. в объектном режиме, выделяем поверхность и кости. нажимаем ctrl + P и выбираем "with empty groups".
 * 6. в pose mode выставляем кости в нужное положение, выделяем их все, нажимаем I, 
 *    выбираем Location. повторяем для остальных кадров.
 * 7. переходим в dope sheet, копируем точки первого кадра, вставляем их в последний.
 * 8. в action editor присваеваем анимации нормальное имя
 * 9. выставляем длительность анимации - 1 кадр
 * 10. сохраняем результат
 * 
 */

public class SeaBoneBlenderAnimation extends SimpleApplication {

    private PointLightShadowRenderer renderer;
    private PointLightShadowFilter filter;
    
    private int SHADOW_MAP_SIZE = 1024;
   

    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.DarkGray);

        
        
        Node scene = (Node)assetManager.loadModel("Blender/low_poly_sea.j3o");        
        rootNode.attachChild(scene);
        
        Spatial animWaves = rootNode.getChild("Plane");                         // !! AnimControl attached to spatial, not to rootNode
        AnimControl control = animWaves.getControl(AnimControl.class);
        AnimChannel channel = control.createChannel();
        channel.setAnim("Waves");
        
        
        

        // Point Light
        PointLight lamp = new PointLight();
        lamp.setColor(ColorRGBA.White.mult(5f));
        lamp.setRadius(20f);
        lamp.setPosition(new Vector3f(4f, 4f, -3f));
        rootNode.addLight(lamp);
        
        
        renderer = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        renderer.setLight(lamp);
        renderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        viewPort.addProcessor(renderer);
        
        filter = new PointLightShadowFilter(assetManager, SHADOW_MAP_SIZE);
        filter.setLight(lamp);
        filter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        filter.setEnabled(false);
        
        // Fpp
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);


      
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        SeaBoneBlenderAnimation app = new SeaBoneBlenderAnimation();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(800, 600);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
