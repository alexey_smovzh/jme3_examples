package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowFilter;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    private PointLightShadowRenderer renderer;
    private PointLightShadowFilter filter;
    
    private int SHADOW_MAP_SIZE = 1024;
   

    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.LightGray);

        
        cam.setLocation(new Vector3f(6.2f, 7f, 4.8f));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
        
        
        
        Geometry plane2 = new Geometry("Plane2", new Box(.01f, 10f, 10f));
        
        Material mat2 = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat2.setTexture("DiffuseMap", assetManager.loadTexture("Blender/diffuse.png"));
        mat2.setTexture("NormalMap", assetManager.loadTexture("Blender/papernormal.png"));        
        mat2.getTextureParam("NormalMap").getTextureValue().setWrap(Texture.WrapMode.Repeat);
        mat2.getTextureParam("DiffuseMap").getTextureValue().setWrap(Texture.WrapMode.Repeat); 
        
        mat2.setBoolean("UseMaterialColors", true);
        mat2.setColor("Ambient", ColorRGBA.White);
        mat2.setColor("Diffuse", ColorRGBA.White);
        mat2.setColor("Specular", ColorRGBA.White);
        mat2.setFloat("Shininess", 8f);
  
        plane2.setMaterial(mat2);
        plane2.setShadowMode(ShadowMode.Receive);
        plane2.setLocalTranslation(-3f, 0f, 0f);
        rootNode.attachChild(plane2);
        
        
        
        
//        Geometry light = new Geometry("Light", new Sphere(10, 10, .5f));
//        Material lightMat = assetManager.loadMaterial("Common/Materials/RedColor.j3m");
//        light.setMaterial(lightMat);
//        light.setShadowMode(ShadowMode.Off);
//        light.setLocalTranslation(new Vector3f(4f, 4f, 4f));
//        rootNode.attachChild(light);
        
        // Ambient
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White);
        rootNode.addLight(ambient);
        
        // Point Light
        PointLight lamp = new PointLight();
        lamp.setColor(ColorRGBA.White);
        lamp.setRadius(5f);
        lamp.setPosition(new Vector3f(4f, 4f, 4f));
        rootNode.addLight(lamp);
        
        
        renderer = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        renderer.setLight(lamp);
        renderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        viewPort.addProcessor(renderer);
        
        filter = new PointLightShadowFilter(assetManager, SHADOW_MAP_SIZE);
        filter.setLight(lamp);
        filter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        filter.setEnabled(false);
        
        // Fpp
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);
       
       
      
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        Main app = new Main();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
