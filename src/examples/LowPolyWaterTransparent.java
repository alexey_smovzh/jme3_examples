/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;

/**
 *
 * @author alex
 */
public class LowPolyWaterTransparent extends SimpleApplication {
    
    Node scene;
    Spatial water;
    Spatial ice;
    
    private static final Vector3f LIGHT_DIRECTION = new Vector3f(-.3f, -.5f, .5f);
    private static final int SHADOW_MAP_SIZE = 1024;
    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        setupLight();        
        
        // Load scene
        scene = (Node)assetManager.loadModel("Blender/water_transparent.j3o");
        rootNode.attachChild(scene);        
        
        water = scene.getChild("Water");
        ice = scene.getChild("Ice");
        ice.setQueueBucket(RenderQueue.Bucket.Opaque);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat.setBoolean("UseMaterialColors", true);
        mat.setColor("Diffuse", new ColorRGBA(.22f, .47f, .47f, .8f));
        mat.setColor("Ambient", ColorRGBA.White);
        mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        water.setMaterial(mat);
        water.setQueueBucket(RenderQueue.Bucket.Translucent);
     
        
        AnimControl control = ice.getControl(AnimControl.class);
        AnimChannel channel = control.createChannel();
        channel.setAnim("my_animation");
        channel.setSpeed(0.2f);
        
    }   
    
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        cam.setLocation(new Vector3f(12f, 2f, 6f));
        cam.lookAt(new Vector3f(-0.9f, 2.5f, -0.4f), Vector3f.ZERO);
        viewPort.setBackgroundColor(ColorRGBA.LightGray);

    }
    
    private void setupLight() {
        
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(LIGHT_DIRECTION);        
        rootNode.addLight(sun);
        
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 2);
        dlsr.setLight(sun);        
        viewPort.addProcessor(dlsr);
        
    }    
    
    public static void main(String[] args) {
         
        LowPolyWaterTransparent app = new LowPolyWaterTransparent();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
