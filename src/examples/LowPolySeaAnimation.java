/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowFilter;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.system.AppSettings;
import java.nio.FloatBuffer;


public class LowPolySeaAnimation extends SimpleApplication {
    
    private PointLightShadowRenderer renderer;
    private PointLightShadowFilter filter;    
    private int SHADOW_MAP_SIZE = 1024;
    
    private Node sea;
    private static final int NUM_MESHES = 1;        // кол-во морей
 
    
    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.DarkGray);

        

        
        sea = (Node)assetManager.loadModel("Blender/low_poly_sea.j3o");    
        rootNode.attachChild(sea);
    
        //
        loadSeaMeshes(sea);
        
        
        // Ambient
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White);
        rootNode.addLight(ambient);     

        // Point Light
        PointLight lamp = new PointLight();
        lamp.setColor(ColorRGBA.White.mult(5f));
        lamp.setRadius(20f);
        lamp.setPosition(new Vector3f(4f, 4f, -3f));
        rootNode.addLight(lamp);
        
        
        renderer = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        renderer.setLight(lamp);
        renderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        viewPort.addProcessor(renderer);
        
        filter = new PointLightShadowFilter(assetManager, SHADOW_MAP_SIZE);
        filter.setLight(lamp);
        filter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        filter.setEnabled(false);
        
        // Fpp
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);


      
    }
    
    //http://hub.jmonkeyengine.org/t/geometry-morphing-interpolating/26247
    private static final ColorRGBA seaColor = new ColorRGBA(4.0507238E-4f, 0.35714608f, 0.51733243f, 1.0f);
    
    Mesh[] meshes = new Mesh[NUM_MESHES];
    int idx = 0;
    
    // Traverse scene graph, find sea meshes by material blue color (seaColor) and add it to meshes[] array
    private void loadSeaMeshes(Node node) {

        for(Spatial spatial : node.getChildren()) {
            if(spatial.matches(Node.class, null)) {
                loadSeaMeshes((Node)spatial);
            } else if(spatial.matches(Geometry.class, null)) {
                Geometry spaGeo = (Geometry)spatial;
                ColorRGBA spaColor = (ColorRGBA)spaGeo.getMaterial().getParam("Diffuse").getValue();
            
                if(spaColor.equals(seaColor)) {
                    meshes[idx] = spaGeo.getMesh();
                    idx++;
                }
            }        
        }
    }    
    
    // Changing vertexes data
    private void makeWaves(Mesh mesh) {
  
        VertexBuffer vertexBuffer = mesh.getBuffer(Type.Position);        
        FloatBuffer floatBuffer = (FloatBuffer)vertexBuffer.getData();
        
        for(int i = 0; i < floatBuffer.capacity(); i++) {
            if(i % 3 == 0) {                                                    // TODO !!!!!
                floatBuffer.put(i, floatBuffer.get(i) + 0.01f);
            } else {
                floatBuffer.put(i, floatBuffer.get(i));
            }
        }
        
        mesh.getBuffer(Type.Position).setUpdateNeeded();
        
    }

    @Override
    public void simpleUpdate(float tpf) {

        for(int i = 0; i < NUM_MESHES; i++) {
            makeWaves(meshes[i]);
        }
        
        sea.updateModelBound();
        
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        LowPolySeaAnimation app = new LowPolySeaAnimation();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(800, 600);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
    
}
