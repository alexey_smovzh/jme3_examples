package examples;

import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.cinematic.Cinematic;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.AbstractCinematicEvent;
import com.jme3.cinematic.events.AnimationEvent;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;

/**
 * test
 * @author alex
 */

/*
 * 1. В блендер копируем/вставляем самолет, поворачиваем по оси Z на 90 градусов. 
 *    Применяем трансформации поворота и положения Object -> Apply -> (Location/Rotation).
 * 2. создаем две вертекс группы Plane, Propeller. Выделяем поверхности лопастей, присваиваем их 
 *    вертекс группе Propeller. Выделяем поверхности самолета, присваем их вертекс группе Plane.
 * 3. Создаем кость, переходим в режим редактирования, по shift+A добавляем еще одну кость. 
 *    Одну распологаем перед винтом, вторую по центру самолета.
 * 4. присваеваем костям имена Propeller и Plane соотв.
 * 5. В режиме редактирования, для кости Propeller выбираем родитея Plane, указываем наследовать 
 *    поворот, размер, положение.
 * 6. Применяем к костям трансформации поворота и положения Object -> Apply -> (Location/Rotation).
 *    В противном случае винт будет вращаться не вокруг центра самолета и самолет будет делать 
 *    петлю по трудноугадываемому радиусу.
 * 7. в объектном режиме, выделяем самолет и кости. нажимаем ctrl + P и выбираем "with empty groups".
 * 8. переходим в режим анимации. в dope sheet -> action editor создаем анимацию Propeller.
 * 9. в Active Keyring Set выставляем записывать только повороты.
 * 10. выставляем на 0 кадр, записываем кейфрейм. Ставим на кадр +4, поворачиваем кость на 90 градусов 
 *     записываем кейфрем, посторяем 4 раза, пока винт не совершит полный оборот. 
 * 11. переводим бегунок на 14-й фрейм, добавляем кейфрейм, удаляем 16-й. Иначе винт будет замирать
 *     в нулевом положении на секунду, анимация будет дергаться.
 * 12. F-Curve Editor выставляем Channel -> Extrapolation Mode -> Make Cyclic
 * 13. В dope sheet -> action editor создаем анимацию Plane. Переходим в вид справа. 
 *     Выставляем на фоновое изображение схему маневра.
 * 14. в Active Keyring Set выставляем записывать повороты и положение.
 * 15. выставляем анимацию на 0 кадр, самолет в положение начала маневра по схеме, 
 *     записываем кейфрейм. проводим самолет по схеме, записывая кейфреймы. Для сглаживания 
 *     анимации маневра добавляем промежуточные кейфреймы.
 * 16. в nla editor переименовываем название треков в Plane и Propeller соотв. 
 *     проверяем их длительность и выставляем количество повторов трека Propeller 
 *     до границ анимации.
 * 17. в треке Plane, удялаем кейфреймы поворота для кости Propeller, оставляем только
 *     кейфреймы положения.
 * 18. экспортируем результат в формат Ogre 
 * 19. конверируем файл .scene в jmonkey в формат .j3o. Открываем файл в SceneComposer. 
 *     Переносим объект с анимациями из глубины дерева файла в начало. Переименовываем 
 *     объект в имя Plane. Сохраняем результат. 
 */


public class PlaneAnimCinematic extends SimpleApplication {

    private Node scene;
    private Node model;
    
    private Cinematic cinematic;
    private MotionPath loop;
    private MotionEvent motion;
    
    
    private static final Vector3f START_POINT = new Vector3f(2f, 3f, -10f);
    private static final Vector3f END_POINT = new Vector3f(2f, 3f, 10f);
    
    
    private static final Vector3f LIGHT_DIRECTION = new Vector3f(-.3f, -.5f, .5f);
    private static final int SHADOW_MAP_SIZE = 1024;
    
    

    @Override
    public void simpleInitApp() {
        
        setupCam();        
        setupLight();
        
        // Load scene
        scene = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        scene.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        rootNode.attachChild(scene);  
        
        // Load plane
        model = (Node)assetManager.loadModel("Blender/Plane/plane.j3o");
        model.setLocalTranslation(START_POINT);
        model.setShadowMode(RenderQueue.ShadowMode.Cast);
        rootNode.attachChild(model);
        
        Spatial plane = model.getChild("Plane");
        
        
 /*       // Motion path
        loop = new MotionPath();
        loop.addWayPoint(START_POINT);      
        
        loop.addWayPoint(new Vector3f(2f, 3f, 2f));
        loop.addWayPoint(new Vector3f(2f, 5f, 4f));
        loop.addWayPoint(new Vector3f(2f, 7f, 2f));
        loop.addWayPoint(new Vector3f(2f, 5f, 0f));
        loop.addWayPoint(new Vector3f(2f, 3f, 2f));
        
        loop.addWayPoint(END_POINT);
        loop.enableDebugShape(assetManager, rootNode);
        
        motion = new MotionEvent(plane, loop);
        motion.setDirectionType(MotionEvent.Direction.PathAndRotation);
        motion.setRotation(new Quaternion().fromAngleAxis(FastMath.HALF_PI, Vector3f.UNIT_Y));
        motion.setSpeed(1f);
         
        cinematic.addCinematicEvent(0, motion);
*/        
        
/*        // Custom animation
        AnimationFactory factory = new AnimationFactory(10, "PlaneLoop");
        factory.addTimeRotationAngles(0, 0f, FastMath.HALF_PI, 0f);
        factory.addTimeTranslation(0, START_POINT);
        
        factory.addTimeTranslation(4, new Vector3f(2f, 3f, 2f));
        factory.addTimeRotationAngles(4, 0f, FastMath.HALF_PI, 0f);
        
        factory.addTimeTranslation(5, new Vector3f(2f, 5f, 4f));
        factory.addTimeRotationAngles(5, 0f, FastMath.HALF_PI, -FastMath.HALF_PI);
        
        factory.addTimeTranslation(6, new Vector3f(2f, 7f, 2f));
        factory.addTimeRotationAngles(6, 0f, FastMath.HALF_PI, -FastMath.PI);        
        
        factory.addTimeTranslation(7, new Vector3f(2f, 5f, 0f));
        factory.addTimeRotationAngles(7, 0f, FastMath.HALF_PI, -FastMath.PI * 3/2);
        
        factory.addTimeTranslation(8, new Vector3f(2f, 3f, 2f));
        factory.addTimeRotationAngles(8, 0f, FastMath.HALF_PI, -FastMath.TWO_PI);        
        
        factory.addTimeTranslation(10, END_POINT);

        AnimControl control = new AnimControl();
        control.addAnim(factory.buildAnimation());
        plane.addControl(control);  
         
         
        cinematic.addCinematicEvent(0, new AnimationEvent(plane, "PlaneLoop", LoopMode.DontLoop));
*/       

        // Cinematic
        cinematic = new Cinematic(rootNode, 40);
        stateManager.attach(cinematic);        
        
        cinematic.addCinematicEvent(0, new AnimationEvent(plane, "Plane", LoopMode.DontLoop));
        cinematic.addCinematicEvent(0, new AnimationEvent(plane, "Propeller", LoopMode.Loop));
        cinematic.addCinematicEvent(40, new endPlaneAnimation(rootNode, plane));
                
        cinematic.play();
        
      
    }
    
    private class endPlaneAnimation extends AbstractCinematicEvent {
        
        Node root;
        Spatial spatial;
        
        public endPlaneAnimation(Node root, Spatial spatial) {
            this.root = root;
            this.spatial = spatial;
        }

        @Override
        protected void onPlay() { root.detachChild(spatial); }

        @Override
        protected void onUpdate(float tpf) {   }

        @Override
        protected void onStop() {  }

        @Override
        public void onPause() { }
        
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.Blue);

        
        cam.setLocation(new Vector3f(6.2f, 7f, 4.8f));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
        
    }
    
    private void setupLight() {    
         
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(LIGHT_DIRECTION);        
        rootNode.addLight(sun);
        
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 2);
        dlsr.setLight(sun);        
        viewPort.addProcessor(dlsr);
        
        
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        PlaneAnimCinematic app = new PlaneAnimCinematic();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
