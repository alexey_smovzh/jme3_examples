/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.system.AppSettings;

/**
 *
 * @author alex
 */
public class AndroidShadow extends SimpleApplication {
    
    Node scene;
    Node model;
    
    private static final Vector3f START_POINT = new Vector3f(2f, 3f, -5f);

    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        setupLight();        
        
        // Load scene
        scene = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        scene.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);        
        rootNode.attachChild(scene);        
        
        // Airplane
        model = (Node)assetManager.loadModel("Blender/Plane/plane.j3o");
        model.setLocalTranslation(START_POINT);
        model.setShadowMode(RenderQueue.ShadowMode.Cast);
        rootNode.attachChild(model);
        
        Spatial plane = model.getChild("Plane");
     
        AnimControl control = plane.getControl(AnimControl.class);
        AnimChannel channel = control.createChannel();
        channel.setAnim("Plane");
        
    }   
    
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        cam.setLocation(new Vector3f(15f, 4f, 0f));
        cam.lookAt(new Vector3f(-0.9f, 2.5f, -0.4f), Vector3f.ZERO);
        viewPort.setBackgroundColor(new ColorRGBA((float)21/255, 0f, (float)142/255, 1f));

    }
    
    private void setupLight() {
        
        DirectionalLight directinal = new DirectionalLight();
        directinal.setDirection(new Vector3f(-0.1f, -0.7f, -1f).normalizeLocal());
        directinal.setColor(new ColorRGBA(0.9f, 1f, 1f, 1.0f).mult(2f));
        rootNode.addLight(directinal);
        
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White.mult(2f));
        rootNode.addLight(ambient);
        
        final DirectionalLightShadowRenderer pssmRenderer = new DirectionalLightShadowRenderer(assetManager, 1024, 4);
        viewPort.addProcessor(pssmRenderer);
                
        pssmRenderer.setLight(directinal);
        pssmRenderer.setLambda(0.55f);
        pssmRenderer.setShadowIntensity(0.55f);
        pssmRenderer.setShadowCompareMode(com.jme3.shadow.CompareMode.Software);
        pssmRenderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
       
        
    }    
    
    public static void main(String[] args) {
         
        AndroidShadow app = new AndroidShadow();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
