/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

/**
 *
 * @author alex
 */
public class ParticleEmitter3DTest extends SimpleApplication {
    
    Node pivot;
    Node scene;
    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        setupLight();        
        
        // Load scene
        scene = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        rootNode.attachChild(scene);        
     
        // Emitter source
        pivot = new Node("Emitter");
        pivot.setLocalTranslation(2.15f, 1.25f, 0.96f);
        scene.attachChild(pivot);        
        
        // 3d particle emitter
        ParticleEmitter3D smokeEmitter = new ParticleEmitter3D();
        smokeEmitter.setEmitter(pivot);                                         // источник дыма
        smokeEmitter.setDirection(new Vector3f(-2f, 4f, -3f));                  // распространяется в высоту на 5f
        smokeEmitter.setModelsPath("Blender/smoke_particles.j3o");
        smokeEmitter.setSpeed(0.05f);
        smokeEmitter.setDispersion(1f);
        smokeEmitter.setSize(0.2f);
        smokeEmitter.setMagnificationRatio(0.05f);
        smokeEmitter.setDensity(1f);
        stateManager.attach(smokeEmitter);
        
        
    }   
    
    @Override
    public void simpleUpdate(float tpf) {
        //scene.move(0f, 0f, 0.01f);
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        cam.setLocation(new Vector3f(12f, 2f, 6f));
        cam.lookAt(new Vector3f(-0.9f, 2.5f, -0.4f), Vector3f.ZERO);
        viewPort.setBackgroundColor(new ColorRGBA((float)21/255, 0f, (float)142/255, 1f));

    }
    
    private void setupLight() {
        
        DirectionalLight directinal = new DirectionalLight();
        directinal.setDirection(new Vector3f(-0.1f, -0.7f, -1f).normalizeLocal());
        directinal.setColor(new ColorRGBA(0.9f, 1f, 1f, 1.0f).mult(2f));
        rootNode.addLight(directinal);
        
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.White.mult(2f));
        rootNode.addLight(ambient);
        
    }    
    
    public static void main(String[] args) {
         
        ParticleEmitter3DTest app = new ParticleEmitter3DTest();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
