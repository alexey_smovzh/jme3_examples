/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.light.DirectionalLight;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;

/**
 *
 * @author alex
 */
public class DinamicLowPolySkyTest extends SimpleApplication {
    
    Node earth;

    private DirectionalLight light; 
    
    private static final int SHADOW_MAP_SIZE = 1024;    
    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        setupLight();        
        
        // Load scene
        earth = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        earth.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        earth.addLight(light);
        rootNode.attachChild(earth);  

        // Sky
        DinamicLowPolySky sky = new DinamicLowPolySky(light);
        sky.setPlanetModels("Blender/DayNightChange/planets.j3o");
        sky.setStarsTexture("Blender/DayNightChange/stars.png");                
        stateManager.attach(sky);
        
    }   
    
    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        
        cam.setLocation(new Vector3f(6.2f, 7f, 4.8f));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);

    }
    
    private void setupLight() {
        
        light = new DirectionalLight();                                                
        
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 4);
        dlsr.setLight(light);        
        viewPort.addProcessor(dlsr);
        
    }    
    
    public static void main(String[] args) {
         
        DinamicLowPolySkyTest app = new DinamicLowPolySkyTest();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
