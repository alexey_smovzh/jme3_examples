/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class StarsAlphaTexture extends SimpleApplication {


    private Node sky;
    private Geometry stars;
    
    private float skySphereRadius = DEFAULT_SKYRADIUS;                          // sky sphere radius
    
    
    private static final ColorRGBA TOP_SKY_COLOR = new ColorRGBA(0f, .027f, .127f, 1f);        
    private static final ColorRGBA BOTTOM_SKY_COLOR = new ColorRGBA(.09f, .42f, 1f, 1f);        
    
    private static final float DEFAULT_SKYRADIUS = 10f;

    
    
    @Override
    public void simpleInitApp() {
        
        flyCam.setMoveSpeed(20f);
        cam.setLocation(new Vector3f(0f, 0f, 5f));
        
        loadModels();
        
    }
    
    // Control for stars sphere rotation
    private class StarsControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            
            spatial.rotate(new Quaternion().fromAngleAxis(.02f * tpf, Vector3f.UNIT_Z));
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {  }
        
    } 
    
    private void setupSkySphere(Node pivot) {
        
        Geometry background = new Geometry("Background", new Sphere(16, 16, skySphereRadius));
        Material material = new Material(assetManager, "MatDefs/GradientSky.j3md");
        material.setColor("TopColor", TOP_SKY_COLOR);        
        material.setColor("BottomColor", BOTTOM_SKY_COLOR);
        material.setFloat("Diameter", skySphereRadius);
        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Front);
        background.setMaterial(material);     
        
        background.setQueueBucket(RenderQueue.Bucket.Sky);  
 
        pivot.attachChild(background);
        
    }
    
    // Setup stars sphere
    private void setupStarSphere(Node pivot) {
        
        stars = new Geometry("Stars", new Sphere(16, 16, 5f));
        stars.setLocalTranslation(Vector3f.ZERO);
        
        // Random X angle
        stars.setLocalRotation(new Quaternion().fromAngles(FastMath.DEG_TO_RAD * (FastMath.rand.nextInt()), 
                                                           FastMath.DEG_TO_RAD * 90, 0f));

                       
        Material material = new Material(assetManager, "MatDefs/StarsAlpha.j3md");
        Texture texture = assetManager.loadTexture(new TextureKey("Blender/starfield.png", true));                
        material.setTexture("Texture", texture);
        material.setFloat("Alpha", 1f);        
        material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);  
        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Front);
        
        stars.setMaterial(material);
        stars.setQueueBucket(RenderQueue.Bucket.Transparent);                           // Change from transparent for clouds
        
        stars.addControl(new StarsControl());
        
        pivot.attachChild(stars);        

    }    
    
    // Load scene
    private void setupScene() {
        
        Spatial earth = (Node)assetManager.loadModel("Blender/low_poly_scene.j3o");
        rootNode.attachChild(earth);  
        
    }
    
    // todo:
    private void loadModels() {
        
        sky = new Node("Sky");
        sky.setLocalTranslation(Vector3f.ZERO);
        rootNode.attachChild(sky);
        
        setupSkySphere(sky);
        setupStarSphere(sky);
//        setupScene();
        // todo: add sky control
        
    }
    
    @Override
    public void simpleUpdate(float tpf) {
         
    }

    public static void main(String[] args) {
        
        StarsAlphaTexture app = new StarsAlphaTexture();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(960, 540);                   // original 540, 960
        settings.setFrameRate(20);
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();
        
    }

}