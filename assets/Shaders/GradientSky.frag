/*
* fragment shader template
*/
uniform vec3 m_TopColor;
uniform vec3 m_BottomColor;
uniform vec2 m_Resolution;

#ifdef TEXTURE
    varying vec2 texCoord;

    uniform sampler2D m_Texture;
    uniform float m_Alpha;

#elif defined(STARS)                                                            

    #define PI 3.1415926535
    #define RADIUS 0.0025
    #define SPEED 0.0005

    varying float time;


    mat2 rot( in float a ) {
        float c = cos(a);
        float s = sin(a);
            return mat2(c,s,-s,c);	
    }

    float noise(vec2 p) {                                                       // noise function
        return sin(p.x) * 0.25 + sin(p.y) * 0.25 + 0.50;
    }

    vec3 getSpaceColor(vec3 dir) {                                              // get the space color
        vec2 uv = vec2(atan(dir.y, dir.x) / (2.0 * PI) + 0.5, mod(dir.z, 1.0));
        uv.x = mod(uv.x+2.0*PI, 1.0);
        uv.x *= 100.0;
        uv.y *= 15.00;
        uv *= rot(1.941611+time*SPEED);
        vec2 center = floor(uv) + 0.5;
        center.x += noise(center*48.6613) * 0.8 - 0.4;
        center.y += noise(center*-31.1577) * 0.8 - 0.4;
        float radius = smoothstep(0.6, 1.0, noise(center*42.487+
                                                  vec2(0.1514, 0.1355)*time)); 
        radius *= RADIUS;
        vec2 delta = uv-center;
        float dist = dot(delta, delta);
        float frac = 1.0-smoothstep(0.0, radius, dist);
        frac *= frac; frac *= frac; frac *= frac;

        return vec3(1)*frac;
    }

#endif


void main() {

    vec2 position = gl_FragCoord.xy / m_Resolution.xy; 

    vec3 gradient = mix(m_BottomColor, m_TopColor, position.y);
    vec4 color = vec4(gradient, 1.0);


    #ifdef TEXTURE                                                              

        vec4 texColor = texture2D(m_Texture, texCoord);
        color = mix(vec4(gradient, 1.0), texColor, texColor.a * m_Alpha);

    #elif defined(STARS)

    	position *= 2.0 - 1.0;
	position.y *= m_Resolution.y / m_Resolution.x;

	vec3 dir = normalize(vec3(position.x*1.0, 1.0, position.y*-1.0));    
	dir.xy *= rot(PI*0.5);
    	
        gradient += getSpaceColor(dir);
        color = vec4(gradient, 1.0);

    #endif

    gl_FragColor = color;                                                       

}

