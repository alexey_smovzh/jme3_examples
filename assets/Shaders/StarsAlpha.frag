/*
* fragment shader template
*/

uniform sampler2D m_Texture;
uniform float m_Alpha;

varying vec2 texCoord;
varying vec4 color;

void main() {
    // Set the fragment color for example to gray, alpha 1.0
    #ifdef ALPHA
        vec4 texColor = texture2D(m_Texture, texCoord);
        gl_FragColor = vec4(texColor.rgb, texColor.a * m_Alpha);
    #else
        gl_FragColor = texture2D(m_Texture, texCoord);  
    #endif
}

