/*
* fragment shader template
*/

uniform sampler2D m_Texture;
varying vec2 texCoord;

#ifdef NIGHT
    uniform vec4 m_Color;
#endif

#ifdef BONFIRELIGHT
    uniform sampler2D m_BonfireLight;
#endif

void main() {
    // Set the fragment color for example to gray, alpha 1.0

    #ifdef NIGHT
        #ifdef BONFIRELIGHT
            gl_FragColor = texture2D(m_BonfireLight, texCoord);
        #else
            vec4 texColor = texture2D(m_Texture, texCoord);
            gl_FragColor = vec4(texColor.rgb * m_Color.rgb, texColor.a);
        #endif
    #else 
        gl_FragColor = texture2D(m_Texture, texCoord);
    #endif

}
