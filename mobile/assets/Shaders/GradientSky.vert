/*
* vertex shader template
*/

uniform mat4 g_WorldViewProjectionMatrix;

attribute vec3 inPosition;
attribute vec2 inTexCoord;

#ifdef TEXTURE
    varying vec2 texCoord;
#endif

#ifdef STARS
    uniform float g_Time;

    varying float time;
#endif

void main() { 
    // Vertex transformation

    gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);

    #ifdef TEXTURE
        texCoord = inTexCoord;
    #endif

    #ifdef STARS
        time = g_Time;
    #endif
}
