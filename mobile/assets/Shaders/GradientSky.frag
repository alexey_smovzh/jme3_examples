/*
* fragment shader template
*/
uniform vec3 m_TopColor;
uniform vec3 m_BottomColor;
uniform vec2 m_Resolution;

#ifdef TEXTURE
    varying vec2 texCoord;

    uniform sampler2D m_Texture;
    uniform float m_Alpha;
#endif

#ifdef STARS
    // by shader https://www.shadertoy.com/view/Md2SR3
    uniform float m_Threshhold;
    varying float time;

    float h(float n) {
        return fract((1.0+cos(n))*415.92653);
    }

    float n2d(vec2 x) {
        float xh = h(x.x*37.0);
        float yh = h(x.y*57.0);
        return fract(xh+yh);
    }

//--
    const float D=8., Z=3.;               // D: duration of advection layers, Z: zoom factor

    #define R(U,d) fract( 1e4* sin( U*mat2(1234,-53,457,-17)+d ) )

    float M(vec2 U, float t) {           // --- texture layer
        vec2 iU = ceil(U/=exp2(t-8.)),              // quadtree cell Id - infinite zoom
                  P = .2+.6*R(iU,0.);                  // 1 star position per cell
        float r = 9.* R(iU,1.).x;                  // radius + proba of star ( = P(r<1) )

	return r > 1. ? 1. :   length( P - fract(U) ) * 8./(1.+5.*r) ;
}

#endif

// Gradient
vec3 c(float d) {
    return (m_TopColor * d / m_Resolution.y) + (m_BottomColor * (1.0 - d / m_Resolution.y));
}

void main() {

    vec2 position = gl_FragCoord.xy / m_Resolution.xy; 

    vec3 gradient = mix(m_BottomColor, m_TopColor, position.y);
    vec4 color = vec4(gradient, 1.0);

// Starfield texture
    #ifdef TEXTURE                                                              
        vec4 texColor = texture2D(m_Texture, texCoord);
        color = mix(vec4(gradient, 1.0), texColor, texColor.a * m_Alpha);
    #endif

// Stars 
    #ifdef STARS
        // from shader https://www.shadertoy.com/view/Md2SR3
/*        float s = n2d(position);
        if(s >= m_Threshhold) {
            s = pow((s-m_Threshhold)/(1.0-m_Threshhold),6.0); 
            gradient += vec3(s);
        }

        color = vec4(gradient, 1.0);
*/
       
       vec3 P = vec3(-1,0,1)/3., T,
   		      t = fract( D + P +.5 )-.5,  // layer time
         	      w = .5+.5*cos(6.28*t);                  // layer weight
       t = t*D+Z;  
    
      // --- prepare the 3 texture layers

        T.x = M(position,t.x),  T.y = M(-position,t.y),  T.z = M(position.yx,t.z);
        T = .03/(T*T);

        // --- texture advection: cyclical weighted  sum

        gradient += dot(w,T);

        color = vec4(gradient, 1.0);
    #endif

// Only sky gradient            
    gl_FragColor = color;


}

